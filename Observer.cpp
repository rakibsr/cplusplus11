/*
 * Observer.cpp
 *
 *  Created on: Oct 23, 2017
 *      Author: dev
 */

#include <iostream>
#include <vector>
#include<algorithm>
#include <stdio.h>
#include <string.h>

using namespace std;
#define info(){cout <<__FILE__<<"::"<< __func__ <<"(" << __LINE__ <<"): ";}
//http://arsenmk.blogspot.com/2012/07/simple-implementation-of-observer.html

// Very simple example. Single listener Single Notifier

/*
1. Listener derived from an abstract base IListener
2. Notifier derived from INotifier. INotifier registers IListener(keep references)
3. each time any change occurs notifier notifies listener by calling listener's common method ie. HandleNotification()
*/

class IListener
{
public:
	virtual void HandleNotification() = 0;
};

class INotifier
{
private:
	IListener *pListener; //3. the notifier keeps reference of listener. Whos to notify?
public:
	virtual void Register(IListener *l)
	{
		pListener = l;
	}
	virtual void UnRegister(IListener *l)
	{
		pListener = NULL;
	}
protected:
	virtual void Notify()// This is private to the child class
	{
		pListener->HandleNotification();
	}
};


class Listener : public IListener //1.all the listerners are derived from an abstract base IListener
{
private:
	virtual void HandleNotification()
	{
		info();
		cout << "Listener: I am notified" << endl;
	}
public:
	Listener()
	{
		info();
		cout << "Listener: obj created" << endl;
	}
};


class Notifier:public INotifier
{
public:
	void DataReceived()
	{
		info();
		cout << "Data received. Notify listener" << endl;
		Notify();
	}
};

//int main()
//{
//	Notifier n;
//	Listener l;
//	n.Register(&l);
//	n.DataReceived();
//	info();
//	cin.get();
//	return 0;
//}


