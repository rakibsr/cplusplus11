//http://thispointer.com/c11-multithreading-part-9-stdasync-tutorial-example/


/*
 * std::async() is a function template that accepts a callback
 * (i.e. function or function object) as an argument and potentially
 *  executes them asynchronously.
 *
 *  First argument in std::async is launch policy, it control the
 *  asynchronous behaviour of std::async. We can create std::async
 *  with 3 different launch policies i.e.
 *  1.std::launch::async --> guarantees async
 *  2.std::launch::deferred --> Non asynchronous behaviour i.e. Function will be called when other thread
 *  will call get() on future to access the shared state.
 *
 *  We can pass any callback in std::async i.e.
	I.   Function Pointer
	II.  Function Object
	III. Lambda Function
 */



/////Calling std::async with function pointer as callback////


#include <string>
#include <chrono>
#include <thread>
#include <future>
#include <iostream>
#include <iomanip>
#include <ctime>

#define info(){cout <<__FILE__<<"::"<< __func__ <<"(" << __LINE__ <<"): ";}

using namespace std;

string fetchDataFromDB(string recvdData)
{
	info();
	// Make sure that function takes 3 seconds to complete
	this_thread::sleep_for(chrono::seconds(3));

	//Do stuff like creating DB Connection and fetching Data
	return "DB_" + recvdData;
}

string fetchDataFromFile(string recvdData)
{
	info();
	// Make sure that function takes 5 seconds to complete
	this_thread::sleep_for(chrono::seconds(5));

	//Do stuff like fetching Data File
	return "File_" + recvdData;
}

//int main()
//{
//	// Get Start Time
//	chrono::system_clock::time_point start = chrono::system_clock::now();
//
//	future<string> resultFromDB = async(launch::async, fetchDataFromDB, "DataFromDB");
//	info();
//	cout << "main thread doing some work meanwhile" << endl;
//
//
//	//Fetch Data from File
//	string fileData = fetchDataFromFile("DataFromFile");
//
//	//Fetch Data from DB
//	// Will block till data is available in future<std::string> object.
//	cout << "main thread getting blocked on future.get()" << endl;
//	string dbData = resultFromDB.get();
//
//	// Get End Time
//	auto end = chrono::system_clock::now();
//
//	//http://en.cppreference.com/w/cpp/chrono/duration/duration_cast
//	auto diff = chrono::duration_cast < chrono::seconds > (end - start).count();
//	cout << "Total Time Taken = " << diff << " Seconds" << endl;
//
//	//Combine The Data
//	string data = dbData + " :: " + fileData;
//
//	//Printing the combined Data
//	cout << "Data = " << data << endl;
//
//	return 0;
//}
