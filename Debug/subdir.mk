################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Condition.cpp \
../Observer.cpp \
../Threadpool_prog.cpp \
../async.cpp \
../async_void.cpp \
../dispatch.cpp \
../future_promise.cpp \
../pool_lizhen.cpp \
../timerEvent.cpp 

OBJS += \
./Condition.o \
./Observer.o \
./Threadpool_prog.o \
./async.o \
./async_void.o \
./dispatch.o \
./future_promise.o \
./pool_lizhen.o \
./timerEvent.o 

CPP_DEPS += \
./Condition.d \
./Observer.d \
./Threadpool_prog.d \
./async.d \
./async_void.d \
./dispatch.d \
./future_promise.d \
./pool_lizhen.d \
./timerEvent.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -v -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


