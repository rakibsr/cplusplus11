
//http://thispointer.com/c11-multithreading-part-8-stdfuture-stdpromise-and-returning-values-from-thread/

/*Actually a std::future object internally stores a value that will be
 * assigned in future and it also provides a mechanism to access that
 * value i.e. using get() member function. But if somebody tries to
 * access this associated value of future through get() function before
 *  it is available, then get() function will block till value is not
 *  available.
 *
 *  std::promise is also a class template and its object promises to set
 *   the value in future. Each std::promise object has an associated
 *   std::future object that will give the value once set by the
 *   std::promise object.

  A std::promise object shares data with its associated std::future object.
 *
 *
 * Workflow:  Thread 1 and Thread 2
 *
 * 1. Thread 1 creates Promise object. Fetch future object from promise object
 * 2. Thread 1 creates thread 2 and pass std::promise. Th2 does some work
 * 3. Th 1 does some work
 * 4. Th 1 get blocked on std::future::get()
 * 5. Th 2 sets value in passed std::promise
 * 6. Th1 gets value in std::future::get() and becomes unblocked
 * 7. Th1 continues working
 */


#include <iostream>
#include <thread>
#include <future>
#include <chrono>

using namespace std;

#define info(){cout <<__FILE__<<"::"<< __func__ <<"(" << __LINE__ <<"): ";}

void thFunc(promise<int> *pObj)
{
	info();
	cout << "New thread created and does some work" << endl;
	this_thread::sleep_for(chrono::milliseconds(200));
	cout << "Setting promise" << endl;
	//5. Th 2 sets value in passed std::promise
	pObj->set_value(30);
}

//
//
//int main()
//{
//	info();
//	//1. creating promise object
//	promise<int> promObj;
//	// and fetching future object from promise
//	future<int> futureObj= promObj.get_future();
//	//2. creating th2 and pass promise to it
//	thread th(thFunc, &promObj);
//	//3 . th1 does some work
//	info();
//	cout<< "main thread doing some work meanwhile" << endl;
//	//4. th1 blocked on future.get() until th2 set promise.
//	cout << "main thread blocked on future.get()" << endl;
//	int retval= futureObj.get();
//
//	/* Note:
//	 * If std::promise object is destroyed before setting the value
//	 *  the calling get() function on associated std::future object
//	 *   will throw exception.
//A part from this, if you want your thread to return multiple values
// at different point of time then just pass multiple std::promise
// objects in thread and fetch multiple return values from thier
// associated multiple std::future objects.
//	 */
//	//6. Th1 gets value in std::future::get() and becomes unblocked
//	// 7. Th1 continues working.
//	cout << "main thread becomes unblocked by getting values in future obj." << endl;
//	cout << "Values at promise from th2: " << retval << endl;
//
//	// Necessary??
//	th.join();
//	cin.get();
//	return 0;
//}
//




















